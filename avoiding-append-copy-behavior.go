package main

import (
	"fmt"
)

func main() {
	// The Slice
	var data []string
	// data := make([]string, 102400) // optimization(1)

	// Capture capacity
	lastCap := cap(data)

	for record := 0; record < 102400; record++ {
		// Append to slice
		data = append(data, fmt.Sprintf("Record: %d", record))
		// data[record] = fmt.Sprintf("Record: %d", record) // optimization(2)

		// This won't trigger since we know the size of data beforehand
		// Thus append will not start making copies all over the place
		if lastCap != cap(data) {
			capChange := float64(cap(data)-lastCap) / float64(lastCap) * 100
			lastCap = cap(data)
			fmt.Printf("Address [%p] | Index [%d] | Capacity[%d - %2.f%%]\n", &data[0], record, cap(data), capChange)
		}
	}
}

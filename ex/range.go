package main

import (
	"fmt"
)

func main() {
	m := map[string]struct{}{
		"alpha": struct{}{},
		"beta":  struct{}{},
	}

	for key, value := range m {
		fmt.Println(key, value)
	}
}

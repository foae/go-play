package main

import (
	"fmt"
	"sort"
)

type command struct {
	name string
}

type byName []command

func (a byName) Len() int           { return len(a) }
func (a byName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byName) Less(i, j int) bool { return a[i].name < a[j].name }

func main() {
	c := []command{
		{"breakpoint"},
		{"help"},
		{"args"},
		{"continue"},
	}

	fmt.Println("Before sorting: ", c)
	sort.Sort(byName(c))
	fmt.Println("After sorting: ", c)
}

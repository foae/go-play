package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func (r rot13Reader) rot13(b byte, basis byte) byte {
	return (b-basis+13)%26 + basis
}

func (r rot13Reader) cryptoByte(b byte) byte {
	if b >= 'A' && b <= 'Z' {
		return r.rot13(b, 'A')
	}

	if b >= 'a' && b <= 'z' {
		return r.rot13(b, 'a')
	}

	return b
}

func (rot rot13Reader) Read(b []byte) (c int, err error) {
	c, err = rot.r.Read(b)

	for i := 0; i < c; i++ {
		b[i] = rot.cryptoByte(b[i])
	}

	return
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}

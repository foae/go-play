package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	file, err := os.Open("test.txt")
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	p := make([]byte, 3200)
	for {
		if n, err := file.Read(p); n > 0 {
			fmt.Printf("\n--------------\n%s\n", p[:n])
		} else if err != nil {
			if err == io.EOF {
				fmt.Println("--------------\nFinished reading the file.")
				break
			} else {
				log.Fatal(err)
			}
		} else {
			// do nothing
		}
	}

	r := strings.NewReader("Hello, Fucker!")
	b := make([]byte, 8)
	for {
		n, err := r.Read(b)
		fmt.Printf("n = %v err = %v b = %v\n", n, err, b)
		fmt.Printf("b[:n] = %q\n", b[:n])
		if err == io.EOF {
			break
		}
	}
}

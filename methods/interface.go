package main

import (
	"math"
)

type Abser interface {
	Abs() float64
}

type MyFuckingFloat float64

type Vertex struct {
	X, Y float64
}

func (f MyFuckingFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

func (v *Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	var a Abser
	f := MyFuckingFloat(-math.Sqrt2)
	v := Vertex{3, 4}

	a = f  // a MyFuckingFloat implements Abser
	a = &v // a *Vertex implements Abser
	a = v  // v is a Vertex (not *Vertex) and does NOT implement Abser
}

package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

// Method function of Vertex struct
func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// Normal function, same functionality
func Abs(v Vertex) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	v := Vertex{6, 7}
	fmt.Println(v.Abs())
	fmt.Println(Abs(Vertex{6, 7}))
}

package main

import "fmt"

type Foo interface {
	foo()
}

// ------------------
type A struct{}

func (a A) foo() {
	fmt.Println("A foo!")
}

func (a A) bar() {
	fmt.Println("A bar!")
}

// ------------------

func callFoo(f Foo) {
	f.foo()
}

func printValue(v interface{}) {
	if v, ok := v.(string); ok {
		fmt.Printf("Value of v is: %v\n", v)
	} else {
		fmt.Printf("Ts ts ts. Can run only strings.\n")
	}
}

func main() {
	var a A
	callFoo(a)
	a.bar()

	// -----

	v := "10"
	printValue(v)
	printValue(10)
}

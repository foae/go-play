package main

import "fmt"

// Calling a method on a nil interface is a run-time error due to no type
// inside the interface tuple to indicate which concrete method to call
type I interface {
	M()
}

func main() {
	var ceva I
	describe(ceva)
	ceva.M()
}

func describe(cumva I) {
	fmt.Printf("(%v, %T)\n", cumva, cumva)
}

package main

import "fmt"

type DamInterface interface {
	M()
}

type SomeStruct struct {
	S string
}

// This method means type T implements the interface I,
// but we don't need to explicitly declare that it does so.
func (t SomeStruct) M() {
	fmt.Println(t.S)
}

func main() {
	var i DamInterface = SomeStruct{"Hello, bo$$"}
	i.M()
}

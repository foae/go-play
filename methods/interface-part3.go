package main

import (
	"fmt"
	"math"
)

type SomeInterface interface {
	M()
}

type SomeStruct struct {
	S string
}

func (t *SomeStruct) M() {
	fmt.Println(t.S)
}

type F float64

func (f F) M() {
	fmt.Println(f)
}

func describe(i SomeInterface) {
	fmt.Printf("(%v, %T)\n", i, i)
}

func main() {
	var i SomeInterface

	i = &SomeStruct{"Ce faci nene"}
	describe(i)
	i.M()

	i = F(math.Pi)
	describe(i)
	i.M()
}

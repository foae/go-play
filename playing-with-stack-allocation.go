package main

//const size = 10 // memory address of s hasn't change
const size = 1024 // the stack will be forced to grow, copying the current stack to a larger one. TWICE!

func main() {
	s := "HELLO FUCKER"
	stackCopy(&s, 0, [size]int{})
}

// Recursively runs, increasing the size of the stack
func stackCopy(s *string, c int, a [size]int) {
	println(c, s, *s)

	c++
	if c == 10 {
		return
	}

	stackCopy(s, c, a)
}

/*
GC will come in and reduce the size of the stack after the func returns
Go prefers copy in this case because it's considered a rare event
*/

package main

import "fmt"

type user struct {
	name, email string
}

// User has a method notify
func (u *user) notify() {
	fmt.Printf("Sending email to %s <%s>...\n", u.name, u.email)
}

// admin will get access to the embedded's structs methods
// if admin overrides with same name, the outer type (admin) takes precedence
type admin struct {
	user  // Embedded !!!
	level string
}

func main() {
	a := admin{
		user:  user{"Bob Burgers", "bob@burgers.com"},
		level: "Expert",
	}

	a.user.notify()
	a.notify()
}

package main

import (
	"fmt"
)

type Notifier interface {
	Notify() error
}

type User struct {
	Name, Email string
}

type Admin struct {
	User  User
	Level string
}

func SendNotification(n Notifier) error {
	return n.Notify()
}

func (u *User) Notify() error {
	fmt.Println("Hi, ", u.Name)
	return nil
}

func (a *Admin) Notify() error {
	fmt.Println("You, ", a.User.Name+" = "+a.Level)
	return nil
}

func main() {
	u := User{"Bob O.", "bob@awesome.com"}
	a := Admin{
		User: User{
			Name:  "Bob GG",
			Email: "ggbob@awesome.com",
		},
		Level: "Underrated",
	}
	SendNotification(&u)
	SendNotification(&a.User) // call on the struct that directly implemets
	SendNotification(&a)
}

package controllers

type Controller struct {
	storage Storage
}

func New(s Storage) *Controller {
	c := Controller{
		storage: s,
	}

	return &c
}

type Payload struct {
	Value string `json:"value"`
}

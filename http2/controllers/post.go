package controllers

import (
	"encoding/json"
	"net/http"
)

func (c *Controller) SetValue(writer http.ResponseWriter, request *http.Request) {
	if request.Method != "POST" {
		writer.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	if err := request.ParseForm(); err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	value := request.FormValue("value")
	c.storage.Put(value)
	writer.WriteHeader(http.StatusOK)

	p := Payload{Value: value}
	if payload, err := json.Marshal(p); err == nil {
		writer.Write(payload)
	}
}

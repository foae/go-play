package handlers

import (
	"encoding/json"
	"net/http"
)

type GreetingResponse struct {
	Payload struct {
		Greeting string `json:"greeting,omitempty"`
		Name     string `json:"name,omitempty"`
		Error    string `json:"error,omitempty"`
	} `json:"payload"`
	Successful bool `json:"successful"`
}

func GreetingHandler(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	if request.Method != "POST" {
		writer.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	var greeting GreetingResponse
	if err := request.ParseForm(); err != nil {
		greeting.Payload.Error = "Bad Request"
		if payload, err := json.Marshal(greeting); err == nil {
			writer.Write(payload)
		}
	}

	name := request.FormValue("name")
	grt := request.FormValue("greeting")

	writer.WriteHeader(http.StatusOK)
	greeting.Successful = true
	greeting.Payload.Name = name
	greeting.Payload.Greeting = grt

	if payload, err := json.Marshal(greeting); err == nil {
		writer.Write(payload)
	}
}

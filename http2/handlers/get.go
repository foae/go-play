package handlers

import (
	"fmt"
	"net/http"
)

func HelloHandler(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "text/plain")
	if request.Method != "GET" {
		writer.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	name := request.URL.Query().Get("name")

	writer.WriteHeader(http.StatusOK)
	writer.Write([]byte(fmt.Sprintf("Hello, %s!", name)))
}

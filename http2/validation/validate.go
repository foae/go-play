package validation

import "errors"

type Verror struct {
	error
}

type Payload struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func ValidatePayload(p *Payload) error {
	if p.Name == "" {
		return Verror{errors.New("Name is required!")}
	}

	if p.Age <= 0 || p.Age >= 100 {
		return Verror{errors.New("Invalid age!")}
	}

	return nil
}

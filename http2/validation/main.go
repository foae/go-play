package main

import (
	"net/http"
)

func main() {
	c := validation.New()
	http.HandleFunc("/", c.Process)
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		panic(err)
	}
}

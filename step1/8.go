package main

import "fmt"

func main() {
	names := [4]string{
		"John",
		"Paul",
		"George",
		"Ringo",
	}

	fmt.Println("name")

	a := names[0:2]
	b := names[1:3]
	fmt.Println(a, b)

	b[0] = "XYX"
	fmt.Println(a, b)
	fmt.Println(names)
}

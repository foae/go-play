package main

import (
	"fmt"
)

func main() {
	var someArr [5]int
	someArr[4] = 100
	fmt.Println(someArr)

	x := [6]float64{1, 23, 45, 67, 89, 91}
	var xx [6]float64
	fmt.Println(x, xx)

	smth()
}

func smth() {
	var a [4]int
	a[0] = 1
	i := a[0] // i == 1
	// a[1] == 0
	_ = i

	// array literal
	b := [2]string{"Smth", "Else"}
	bb := [...]string{"Smth", "Else", "More"} // let the compiler count the elements for us
	_ = b
	_ = bb

	// Differences between arrays and slices
	lettersArray := [3]string{"b", "c", "d"}

	// Slice literal
	lettersSlice := []string{"a", "b", "c"}

	var lettersBlankSlice []string
	lettersBlankSlice = make([]string, 5, 5)
	lettersBlankSlice2 := make([]string, 5) // same as above, short version
	_ = lettersBlankSlice2

	fmt.Println(lettersArray, lettersSlice, lettersBlankSlice)
}

// array := [...]float64{7.0, 8.5, 9.1}
// x := ssum(&array)
func ssum(a *[3]float64) (sum float64) {
	for _, v := range *a {
		sum += v
	}

	return
}

/*
- a slice does not have a length specified
*/

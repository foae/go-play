package main

import "fmt"

//var ceva = []int{1, 2, 4, 8, 16, 32, 64, 128}
var ceva = []int{1, 2, 4, 8, 16, 32, 64, 128}

func main() {
	for i, v := range ceva {
		fmt.Printf("2**%d = %d\n", i, v)
	}
}

package main

import (
	"fmt"
)

type Vertex struct {
	Lat, Long float64
}

var m map[string]Vertex

func main() {
	m = make(map[string]Vertex)
	m["BellzThemWhistlez"] = Vertex{48.65, -43.89}
	fmt.Println(m["BellzThemWhistlez"])
}

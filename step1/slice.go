package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	s := [3]string{"a", "b", "c"}
	t := make([]string, len(s), cap(s))
	copy(t, s[:])
	fmt.Println(s, t)
}

//
func appendByte(slice []byte, data ...byte) []byte {
	m := len(slice)    // get the length of slice
	n := m + len(data) // make room

	if n > cap(slice) { // if necessary, reallocate
		// simply allocate double than it's really needed, for future growth
		newSlice := make([]byte, (n+1)*2)
		copy(newSlice, slice)
		slice = newSlice
	}

	slice = slice[0:n]
	copy(slice[m:n], data)
	return slice
}

// p := []byte{2,3,4}
// p = appendByte(p, 7, 9, 11)
// p == []byte{2,3,4,7,9,11}

func appendSlices() {
	a := []string{"Gigi", "Vasile"}
	b := []string{"Gogu", "Ion", "Cip"}

	// equivalent of append(a, b[0], b[1], b[2])
	a = append(a, b...)

}

// returns a new slice []int holding only the elements
// of s that satisfy fn()
func Filter(s []int, fn func(int) bool) []int {
	var p []int // nil
	for _, val := range s {
		if fn(val) {
			p = append(p, val)
		}
	}

	return p
}

func Filter2(incomingSlice []int, fnToApply func(int) bool) []int {
	var temp []int
	for _, value := range incomingSlice {
		if fnToApply(value) {
			temp = append(temp, value)
		}
	}

	return temp
}

/*
Because sometimes using (referencing) large files will keep them in memory,
even if we're using slices, it is wise
to copy(dst, trg) the slice and return that
*/
func CopyDigits(filename string) []byte {
	b, _ := ioutil.ReadFile(filename)
	b = digitRegexp.Find(b)
	c := make([]byte, len(b))

	// do the dew
	copy(c, b)
	return c
}

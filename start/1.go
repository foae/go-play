package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {
	sum := 10
	for sum < 100 {
		sum += sum
		fmt.Println(sum)
	}

	fmt.Println("Final sum: ", sum)

	fmt.Println("GO runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X")
	case "linux":
		fmt.Println("Linux")
	default:
		fmt.Println("%s", os)
	}

	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Fuck off, morning")
	case t.Hour() < 17:
		fmt.Println("Fuck off, afternoon")
	default:
		fmt.Println("Bye")
	}

	defer fmt.Println("sucks")
	fmt.Println("it")
}

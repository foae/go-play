package main

import (
	"fmt"
)

var coll = [8]string{"Donut", "Fries", "Junk", "Cookie", "Donut", "Fries", "Fruits", "Fruits"}

func main() {
	// Make a Map with Strings as Keys and Ints as values
	localMap := make(map[string]int)
	for _, word := range coll {
		_, ok := localMap[word]
		if ok {
			localMap[word]++
		} else {
			localMap[word] = 1
		}
	}

	fmt.Println(localMap)
}

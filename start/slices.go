package main

import "fmt"

// len - the number of elements it contains
// cap - the capacity of the underlying array

// Slices can contain any type, including other slices

func main() {
	// Creates an array && builds a slice that references it
	q := []int{2, 4, 6, 8, 10}
	s := []struct {
		i int
		b bool
	}{
		{2, false},
		{3, true},
		{4, true},
	}

	// nil slice
	var poorNilSlice []int
	_ = poorNilSlice

	b := make([]int, 0, 5) // len=0, cap=5
	b = b[:cap(b)]         // len=5, cap=5
	b = b[1:]              // len=4, cap=4
	_ = b

	fmt.Println(q, s)
	fmt.Println(len(q[1:3]), cap(q))
}

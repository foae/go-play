package main

import "fmt"

func main() {
	i, j := 42, 2701
	_ = j

	p := &i
	fmt.Println(*p, i)
	*p = 23
	fmt.Println(*p, i)
}

package main

import (
	"fmt"
	"math"
)

// Functions are values too and can be passed around just loke other values
// May be used a function arguments and (both) return values

// Functions might be closures. A closure is a function value that references
// variables from outside its body. The function may access and assign to the
// referenced variables; the function is "bound" to the variables
// E.g.: adder returns a closure; each closure is bound to its own `sum` variable
func adder() func(int) int {
	sum := 0
	return func(x int) int {
		sum += x
		return sum
	}
}

func main() {
	hypot := func(x, y float64) float64 {
		return math.Sqrt(x*x + y*y)
	}

	fmt.Println(hypot(5, 12))
	fmt.Println(compute(hypot))
	fmt.Println(compute(math.Pow))
	fmt.Println("----------------")

	pos, neg := adder(), adder()
	for i := 0; i < 10; i++ {
		fmt.Println(pos(i), neg(-2*i))
	}
}

func compute(fn func(float64, float64) float64) float64 {
	return fn(3, 4)
}

package main

import "fmt"

// Unnamed types are defined by type literal []int
type CustomType1 []int
type CustomType2 []int

func main() {
	var s1 CustomType1
	var s2 CustomType2

	var s3 []int = s1

	fmt.Println(s1, s2, s3)
}

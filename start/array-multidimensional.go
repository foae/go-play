package main

import (
	"fmt"
	"reflect"
)

func main() {
	s := make([][]int, 2)
	fmt.Println(len(s), cap(s), &s[0])

	s[0] = []int{1, 2, 3}
	fmt.Println(len(s[0]), cap(s[0]), &s[0][0])

	s[1] = make([]int, 3, 5)
	fmt.Println(len(s[1]), cap(s[1]), &s[1][0])

	// Slice [1:] returns [[0,0,0]]
	// [0] returns [0 0 0]
	// Slice again [2:3] to get only [0]
	fmt.Println(s[1:][0][2:3])

	fmt.Println("---------------")

	array := [...]int{1, 2, 3}
	slice := [][]int{
		{3, 2, 1},
		{3, 2, 1},
	}
	fmt.Println(reflect.TypeOf(array), reflect.TypeOf(slice), slice)
}

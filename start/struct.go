package main

import (
	"fmt"
)

type Vertex struct {
	X, Y int
}

func main() {
	v := Vertex{1, 9}
	v.X = 7888
	fmt.Println(v)
	itt(v)
}

func itt(someStruct Vertex) {
	fmt.Println(someStruct.fnOnVertex())
}

func (smth Vertex) fnOnVertex() string {
	return "This struct now has its own method"
}

package main

import (
	"fmt"
)

// A function that returns a function (closure) that returns an int
// 0 1 1 2 3 5 8 13 21 34
func fibonacci1() func() int {
	fifi := 0
	return func() int {
		first, second := 0, 1
		for i := 0; i < fifi; i++ {
			first, second = second, first+second
		}
		fifi++
		return first
	}
}

func fibonacci2() func() int {
	i := 0
	a := 0
	b := 1
	return func() int {
		i = a
		a = a + b
		b = i
		return i
	}
}

func main() {
	f := fibonacci2()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}

package main

import "fmt"

/*
Unordered collection of key-value pairs
aka associative array, hash table, dictionary
*/

type Vertex struct {
	Lat, Long float64
}

// someMap is a map with strings as keys and values of type/structure Vertex
var someMap1 map[string]Vertex

// Map literals are like struct literals, but the keys are required
var mapLiteral = map[string]Vertex{
	"SomeKey":    Vertex{13, 45},
	"AnotherKey": Vertex{46, 98},
}

// If the top-level type is just a type name, it may be omitted from the elements of the literal
var mapLiteralOmit = map[string]Vertex{
	"Bell Labs": {40.68433, -74.39967},
	"Google":    {37.42202, -122.08408},
}

func main() {
	anotherMap := make(map[string]Vertex)
	anotherMap["FirstElem"] = Vertex{23.56, -56.98}
	anotherMap["SecondElem"] = Vertex{512.563, -543.543}

	mutMap := make(map[string]int)
	mutMap["Answer"] = 54
	delete(mutMap, "Answer")
	elem, ok := mutMap["Answer"]
	fmt.Println("The value ", elem, " is present in mutMap? ", ok)

	fmt.Println(someMap1)
	fmt.Println(anotherMap)
	fmt.Println(anotherMap["FirstElem"].Long)
	fmt.Println(mapLiteral)
}

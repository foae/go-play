package main

import "fmt"

type notifier interface {
	notify()
}

func sendNotification(n notifier) {
	n.notify()
}

type user struct {
	name, email string
}

func (u *user) notify() {
	fmt.Println("Pointer receiver of the inner type > ", u.name, u.email)
}

type admin struct {
	user  // !!! EMBEDDING
	level string
}

// The second we implement this, any sequential call will be routed to the following
// Effectively overriding the inner type's method
// func (a admin) notify() {
// 	fmt.Println("Value received admin > ", a.name, a.email)
// }

func (a *admin) notify() {
	fmt.Println("Pointer received ADMIN > ", a.name, a.email)
}

func main() {
	ad := admin{
		user: user{
			name:  "john smith",
			email: "john@yahoo.com",
		},
		level: "super",
	}

	// The embedded inner type's implementation of the interface is promoted
	// By simply doing the honor of respecting the inner type's concrete implementation
	// and by that - passing a reference to the pointer receiver
	sendNotification(&ad)
	ad.notify()      // same as above
	ad.user.notify() // same as above. be specific to overcome overriding

	// sendNotification(ad) // activate if outer type's method is activated also

}

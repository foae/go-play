package main

import (
	"fmt"
)

// Create a new channel: make
// Send a value through the channel: <-value
// Both send & receive block until the sender and receiver are ready
func main() {
	messages := make(chan string)

	go func() { messages <- "ping" }()

	receivedMsg := <-messages
	fmt.Println(receivedMsg)
}

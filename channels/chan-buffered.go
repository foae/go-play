package main

import (
	"fmt"
)

// Due to buffering, we can send values in the channel without
// a corresponding concurrent receiver
func main() {
	messages := make(chan string, 2)
	messages <- "buffered"
	messages <- "chan chan motherfucker"

	fmt.Println(<-messages)
	fmt.Println(<-messages)
}

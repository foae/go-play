// package main

// import (
// 	"fmt"
// 	"io/ioutil"
// )

// type Page struct {
// 	Title string
// 	Body  []byte
// }

// // Save the page's body to a txt file
// func (p *Page) save() error {
// 	filename := p.Title + ".txt"
// 	return ioutil.WriteFile(filename, p.Body, 0600)
// }

// // Load back the page
// // returns a pointer to a Page literal constructed with the proper structure
// func loadPage(title string) (*Page, error) {
// 	filename := title + ".txt"
// 	body, err := ioutil.ReadFile(filename) // returns []byte, error
// 	if err != nil {
// 		return nil, err
// 	}

// 	return &Page{Title: title, Body: body}, nil
// }

// func main() {
// 	p1 := &Page{Title: "TestPage", Body: []byte("This is a supermarket sample")}
// 	p1.save()
// 	p2, _ := loadPage("TestPage")
// 	fmt.Println(p2)
// }
